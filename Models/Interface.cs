﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewCourseProject.Models
{
    public interface Interface
    {
        public string answer { get; set; }
        public string filepath { get; set; }
        public string Value { get; set; }
        public string Proverka { get; set; }
    }
}
