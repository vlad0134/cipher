﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using Word = Microsoft.Office.Interop.Word;

namespace NewCourseProject.Models
{
    public class Decipher: Interface
    {
        public string answer { get; set; }
        public string filepath { get; set; }
        public string Value { get; set; }   
        public string Proverka { get; set; }
        public void ToDecipher( IFormFile file, string key)
        {
            if (file != null && file.Length > 0)
            {

                this.Value = "decip";
                string texttodec = "";
                string alfabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

                if (file != null && file.Length > 0 && file.FileName.EndsWith(".txt"))
                {
                    string fileName = Path.GetFileName(file.FileName);
                    string filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/files", fileName);
                    this.filepath = filePath;
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(fileStream);
                    }
                   
                    FileStream stream = new FileStream(filePath, FileMode.Open);
                    StreamReader reader = new StreamReader(stream);
                    texttodec = reader.ReadToEnd();
                    stream.Close();
                    reader.Dispose();

                    File.Delete(filepath);

                    Dictionary<string, int> proverka = new Dictionary<string, int>();
                    Dictionary<int, string> proverka2 = new Dictionary<int, string>();
                    for (int i = 0; i < alfabet.Length; i++)
                    {
                        proverka.Add(alfabet[i].ToString(), i);
                        proverka2.Add(i, alfabet[i].ToString());
                        proverka2.Add(i - 33, alfabet[i].ToString());
                    }
                    int d = 0;
                    for (int i = 0; i < texttodec.Length; i++)
                    {
                        if (alfabet.Contains(texttodec[i]))
                        {
                            int znach = proverka[texttodec[i].ToString()];
                            znach -= proverka[key[d % key.Length].ToString()];
                            texttodec = texttodec.Remove(i, 1);
                            texttodec = texttodec.Insert(i, proverka2[znach]);
                            d++;
                        }
                    }
                }
                if (file != null && file.Length > 0 && file.FileName.EndsWith(".docx"))
                {
                    string filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/files", file.FileName);
                    this.filepath = filePath;

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(fileStream);
                    }
                    Word.Application app = new Word.Application
                    {
                        Visible = false
                    };
                    object fileName = filePath;
                    app.Documents.Open(ref fileName);
                    Word.Document doc = app.ActiveDocument;

                    for (int i = 1; i <= doc.Paragraphs.Count; i++)
                    {
                        string parText = doc.Paragraphs[i].Range.Text;
                        texttodec += parText;
                    }
                    app.Documents.Close();
                    app.Quit();

                    File.Delete(filePath);

                    Dictionary<string, int> proverka = new Dictionary<string, int>();
                    Dictionary<int, string> proverka2 = new Dictionary<int, string>();
                    for (int i = 0; i < alfabet.Length; i++)
                    {
                        proverka.Add(alfabet[i].ToString(), i);
                        proverka2.Add(i, alfabet[i].ToString());
                        proverka2.Add(i - 33, alfabet[i].ToString());
                    }
                    int d = 0;
                    for (int i = 0; i < texttodec.Length; i++)
                    {
                        if (alfabet.Contains(texttodec[i]))
                        {
                            int znach = proverka[texttodec[i].ToString()];
                            znach -= proverka[key[d % key.Length].ToString()];
                            texttodec = texttodec.Remove(i, 1);
                            texttodec = texttodec.Insert(i, proverka2[znach]);
                            d++;
                        }
                    }
                }
                this.answer = texttodec;
            }         
        }       
    }

    public class Cipher : Interface
    {
        public string answer { get; set; }
        public string filepath { get; set; }
        public string Value { get; set; }
        public string Proverka { get; set; }

        public void ToCipher(IFormFile file, string key)
        {
            this.Value = "tocip";
            string texttodec = "";
            string alfabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

            if (file != null && file.Length > 0 && file.FileName.EndsWith(".txt"))
            {
                string fileName = Path.GetFileName(file.FileName);
                string filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/files", fileName);
                this.filepath = filePath;
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                    fileStream.Close();
                }

                FileStream stream = new FileStream(filePath, FileMode.Open);
                
                StreamReader reader = new StreamReader(stream);
                texttodec = reader.ReadToEnd();
                stream.Close();
                reader.Dispose();
                File.Delete(filepath);
                Dictionary<string, int> proverka3 = new Dictionary<string, int>();
                Dictionary<int, string> proverka4 = new Dictionary<int, string>();
                for (int i = 0; i < alfabet.Length; i++)
                {
                    proverka3.Add(alfabet[i].ToString(), i);
                    proverka4.Add(i, alfabet[i].ToString());
                    proverka4.Add(i + 33, alfabet[i].ToString());
                }
                int ddd = 0;
                for (int i = 0; i < texttodec.Length; i++)
                {
                    if (alfabet.Contains(texttodec[i]))
                    {
                        int znach = proverka3[texttodec[i].ToString()];
                        znach += proverka3[key[ddd % key.Length].ToString()];
                        texttodec = texttodec.Remove(i, 1);
                        texttodec = texttodec.Insert(i, proverka4[znach]);
                        ddd++;
                    }
                }
            }
            if (file != null && file.Length > 0 && file.FileName.EndsWith(".docx"))
            {
                string filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/files", file.FileName);
                this.filepath = filePath;

                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
                Word.Application app = new Word.Application
                {
                    Visible = false
                };
                object fileName = filePath;
                app.Documents.Open(ref fileName);
                Word.Document doc = app.ActiveDocument;

                for (int i = 1; i <= doc.Paragraphs.Count; i++)
                {
                    string parText = doc.Paragraphs[i].Range.Text;
                    texttodec += parText;
                }
                app.Documents.Close();
                app.Quit();

                File.Delete(filePath);

                Dictionary<string, int> proverka3 = new Dictionary<string, int>();
                Dictionary<int, string> proverka4 = new Dictionary<int, string>();
                for (int i = 0; i < alfabet.Length; i++)
                {
                    proverka3.Add(alfabet[i].ToString(), i);
                    proverka4.Add(i, alfabet[i].ToString());
                    proverka4.Add(i + 33, alfabet[i].ToString());
                }
                int ddd = 0;
                for (int i = 0; i < texttodec.Length; i++)
                {
                    if (alfabet.Contains(texttodec[i]))
                    {
                        int znach = proverka3[texttodec[i].ToString()];
                        znach += proverka3[key[ddd % key.Length].ToString()];
                        texttodec = texttodec.Remove(i, 1);
                        texttodec = texttodec.Insert(i, proverka4[znach]);
                        ddd++;
                    }
                }
            }
                this.answer= texttodec;
        }
        
        public void ToCipherUserText(string texttodec, string key)
        {
            string alfabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            if (texttodec!="")
            {
                Dictionary<string, int> proverka3 = new Dictionary<string, int>();
                Dictionary<int, string> proverka4 = new Dictionary<int, string>();
                for (int i = 0; i < alfabet.Length; i++)
                {
                    proverka3.Add(alfabet[i].ToString(), i);
                    proverka4.Add(i, alfabet[i].ToString());
                    proverka4.Add(i + 33, alfabet[i].ToString());
                }
                int ddd = 0;
                for (int i = 0; i < texttodec.Length; i++)
                {
                    if (alfabet.Contains(texttodec[i]))
                    {
                        int znach = proverka3[texttodec[i].ToString()];
                        znach += proverka3[key[ddd % key.Length].ToString()];
                        texttodec = texttodec.Remove(i, 1);
                        texttodec = texttodec.Insert(i, proverka4[znach]);
                        ddd++;
                    }
                }
            }
            this.answer = texttodec;
        }
    }
    public class Saver
    {
        public string answer { get; set; }
        public string filepath { get; set; }
        public string Value { get; set; }
        public void ToSaveFileDec(string directory, string filename, string format, string answer, string path)
        {
            if (format == ".txt")
            {
                using StreamWriter sw = new StreamWriter(directory + "/" + filename + format, false, System.Text.Encoding.Default);
                sw.Write(answer);
                sw.Close();
            }
            if (format==".docx")
            {
                Word._Application app = new Word.Application
                {
                    Visible = false
                };
                var doc = app.Documents.Add();
                var r = doc.Range();             
                r.Text = answer;
                object fullfileName = directory + "/" + filename + format;
                doc.SaveAs(ref fullfileName);
                app.Quit();
            }
        }
        public void ToSaveFileEn(string directory, string filename, string format, string answer, string path)
        {
            try 
            { 
            if (format == ".txt")
            {
                using StreamWriter sw = new StreamWriter(directory + "/" + filename + format, false, System.Text.Encoding.Default);
                sw.Write(answer);
                sw.Close();
            }
                if (format == ".docx")
                {
                    Word._Application app = new Word.Application
                    {
                        Visible = false
                    };
                    var doc = app.Documents.Add();
                    var r = doc.Range();
                    r.Text = answer;
                    object fullfileName = directory + "/" + filename + format;
                    doc.SaveAs(ref fullfileName);
                    app.Quit();
                }
            }
            catch(Exception ex)
            {

            }
        }
    }
}