﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using NewCourseProject.Models;

namespace NewCourseProject.Controllers
{
    public class HomeController : Controller
    {
        IWebHostEnvironment environment;
        Decipher decipher;
        Saver saver;
        Cipher cipher;
        
        public HomeController(IWebHostEnvironment environment, Decipher decipher, Saver saver, Cipher cipher)
        {
            this.decipher = decipher;
            this.environment = environment;
            this.saver = saver;
            this.cipher = cipher;
        }
        public IActionResult Index()
        {        
            return View("Index");
        }
        public IActionResult Encoder()
        {
            return View("Encoder");
        }
        public IActionResult Decoder()
        {        
            return View("Decoder");
        }
        public IActionResult UserText()
        {
            return View("Usertext");
        }
        public IActionResult ToDecode(IFormFile file, string key)
        {
            if (file == null || key == null || key == "")
            {
                decipher.Proverka = "Не все поля были корректно заполнены!";
                return View("Decoder",decipher);
            }
            else
            {
                decipher.ToDecipher(file, key);
                return View("Answer", decipher);
            }
        }
        public IActionResult ToEncode(IFormFile file, string key)
        {
            if (file == null || key == null || key == "")
            {
                cipher.Proverka = "Не все поля были корректно заполнены!";
                return View("Encoder",cipher);
            }
            else
            {
                cipher.ToCipher(file, key);
                return View("Answer", cipher);
            }
        }
        public IActionResult ToEncodeUserText(string texttodec, string key)
        {
            if (texttodec == null || texttodec == "" || key == null || key == "")
            {
                cipher.Proverka = "Не все поля были корректно заполнены!";
                return View("Usertext", cipher);
            }
            else
            {
                cipher.ToCipherUserText(texttodec, key);
                return View("Answer", cipher);
            }
        }
        public IActionResult SaveFile(string directory, string filename, string format, string value)
        {
            if (directory==""||directory==null|| filename==""|| filename==null|| format==""||format==null)
            {                
                return View("Error");         
            }
            else
            {
                if (value == "decip")
                {
                    saver.ToSaveFileDec(directory, filename, format, decipher.answer, decipher.filepath);
                }
                else
                {
                    saver.ToSaveFileEn(directory, filename, format, cipher.answer, cipher.filepath);
                }
                return View("Index");
            }
        }
    }
}